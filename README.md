# Deploy Image

This image can be used as image in `.gitlab-ci.yml' files as base when deployments are done via ssh or rsync.

## Example

```yaml
image: docker.gitlab.gwdg.de/subugoe/deploy-image:main

default:
  before_script:
    - echo "$CI_DEV_PRIVATE_KEY" | tr -d '\r' | ssh-add -

deploy-job:
  stage: deploy
  script:
    - ssh $DEV_SSH_LOGIN "rm -r /"
    - scp -r $PWD/* $DEV_SSH_LOGIN:/
```
